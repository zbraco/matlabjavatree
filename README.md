# README #

This package provides the capability to easily create tree controls for an application or user interface using a simple MATLAB® object-oriented interface. This package includes and wraps the Java® code necessary to implement a tree, enabling you to create the tree using only MATLAB® code, for a set of included features.

This code is from MathWorks.

* Please see readme.pdf for more details.
* Version 1.0
